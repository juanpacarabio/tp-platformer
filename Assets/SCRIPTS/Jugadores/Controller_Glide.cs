using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Glide : Controller_Player
{
    public int jumpCounter = 0;

    public override bool IsOnSomething()
    {
        return Physics.BoxCast(transform.position, new Vector3(transform.localScale.x * 0.9f, transform.localScale.y / 3, transform.localScale.z * 0.9f), Vector3.down, out downHit, Quaternion.identity, downDistanceRay);
    }

    protected override void Update()
    {
        base.Update();
        if (Input.GetKeyDown(KeyCode.Space)&&puedeGlide==true)
        {
            StartCoroutine(Glide());
        }
    }

    IEnumerator Glide()
    {
        rb.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
        yield return new WaitForSecondsRealtime(1F);
        puedeGlide = false;
        rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
        yield break;

    }
}
