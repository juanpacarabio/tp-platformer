using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Time : Controller_Player
{
    public GameObject relojBase;
    public GameObject reloj01;
    public GameObject reloj02;
    public GameObject reloj03;
    public GameObject reloj04;
    public GameObject slowdown;
    public GameManager Manager= GameManager.instance;
    
    protected override void Update()
    {
        base.Update();
        if (Input.GetKeyDown(KeyCode.F) && Manager.puedeSlowdown==true)
        {
            StartCoroutine(PararTiempo());
        }


    }
    

    IEnumerator PararTiempo()
    {
        slowdown.SetActive(true);
        reloj01.SetActive(true);
        Time.timeScale = 0.5f;
        yield return new WaitForSecondsRealtime(1f);
        reloj01.SetActive(false);
        reloj02.SetActive(true);
        Time.timeScale = 0.6f;
        yield return new WaitForSecondsRealtime(1f);
        reloj02.SetActive(false);
        reloj03.SetActive(true);
        Time.timeScale = 0.7f;
        yield return new WaitForSecondsRealtime(1f);
        reloj03.SetActive(false);
        reloj04.SetActive(true);
        Time.timeScale = 0.8f;
        yield return new WaitForSecondsRealtime(1f);
        reloj04.SetActive(false);
        Time.timeScale = 1;
        slowdown.SetActive(false);
        yield break;
        
    }
}
