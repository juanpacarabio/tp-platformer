using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drunken : Controller_Player
{
    public float maxSpeed = 10f; 
    public float acceleration = 1f; 
    public float maxTiltAngle = 45f; 
    public float tiltSpeed = 1f; 
  
    private float currentSpeed = 0f; 
    private float currentTilt = 0f; 


    protected override void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {

            rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);

        }
    }

    public override void FixedUpdate ()
    {
                
        float moveHorizontal = Input.GetAxis("Horizontal");

       
        float accelerationInput = Mathf.Abs(moveHorizontal);

       
        if (moveHorizontal > 0)
        {
            currentSpeed += acceleration * accelerationInput * Time.deltaTime;
        }
        else if (moveHorizontal < 0)
        {
            currentSpeed -= acceleration * accelerationInput * Time.deltaTime;
        }
        else
        {
            
            currentSpeed *= 0.95f;
        }

        currentSpeed = Mathf.Clamp(currentSpeed, -maxSpeed, maxSpeed);

        Vector3 newPosition = transform.position + new Vector3(currentSpeed * Time.deltaTime, 0, 0);
        rb.MovePosition(newPosition);

        currentTilt = Mathf.Lerp(0, maxTiltAngle, Mathf.Abs(currentSpeed) / maxSpeed);

        transform.rotation = Quaternion.Euler(0, 0, -currentTilt * Mathf.Sign(currentSpeed));
    }

   
}
