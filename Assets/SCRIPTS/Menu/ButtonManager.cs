using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{
    public GameObject start;
    public GameObject creditos;
    public GameObject menuP;
    public GameObject teclas;

    public void Start()
    {
        start.SetActive(true);
        creditos.SetActive(false);
        menuP.SetActive(false);
        teclas.SetActive(false);
    }

    public void Creditos()
    {
        start.SetActive(false);
        creditos.SetActive(true);
        menuP.SetActive(false);
        teclas.SetActive(false);
    }

    public void MenuPrincipal()
    {
        start.SetActive(false);
        creditos.SetActive(false);
        menuP.SetActive(true);
        teclas.SetActive(false);
    }
    public void Controles()
    {
        start.SetActive(false);
        creditos.SetActive(false);
        menuP.SetActive(false);
        teclas.SetActive(true);
    }
    public void VolverCreditos()
    {
        creditos.SetActive(false);
        start.SetActive(true);
        menuP.SetActive(false);
        teclas.SetActive(false);
    }

    public void VolverTeclas()
    {
        teclas.SetActive(false);
        creditos.SetActive(false);
        start.SetActive(false);
        menuP.SetActive(true);
    }


    public void ComenzarPartida()
    {
        //cargar la escena que sea necesaria.
        SceneManager.LoadScene(1);
    }

    public void Salir()
    {
        Application.Quit();
    }

    public void VolverStart()
    {
        menuP.SetActive(false);
        teclas.SetActive(false);
        creditos.SetActive(false);
        start.SetActive(true);
    }



}
