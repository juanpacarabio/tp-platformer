using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformaDesaparece : MonoBehaviour
{
     public float tiempoAparecer = 2f; // How long the platform stays visible
    public float timepoDesaparecer = 1f; // How long the platform stays invisible

    private Renderer platformRenderer; // Reference to the platform renderer
    private MeshCollider platformCollider;
    private bool isVisible = true; // Flag to track whether the platform is visible

    void Start()
    {
        // Get a reference to the platform renderer
        platformRenderer = GetComponent<Renderer>();
        platformCollider = GetComponent<MeshCollider>();
        // Start the coroutine that controls the platform's visibility
        StartCoroutine(ToggleVisibility());
    }

    IEnumerator ToggleVisibility()
    {
        while (true)
        {
            // Wait for the appear time to elapse
            yield return new WaitForSeconds(tiempoAparecer);

            // Toggle the platform's visibility
            platformRenderer.enabled = isVisible;
            platformCollider.enabled = isVisible;
            isVisible = !isVisible;

            // Wait for the disappear time to elapse
            yield return new WaitForSeconds(timepoDesaparecer);

            // Toggle the platform's visibility again
            platformRenderer.enabled = isVisible;
            platformCollider.enabled = isVisible;
            isVisible = !isVisible;
        }
    }
}
