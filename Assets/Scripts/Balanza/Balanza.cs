using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balanza : MonoBehaviour
{
    public GameObject izquierda;
    public GameObject derecha;
    public GameObject balanza;


    public bool pesoIzq;
    public bool pesoDer;



    bool esRotableIZQ;
    bool esRotableDER;

    public float velocidad;
    Vector3 direccion = new Vector3();
    Vector3 limiteIzq = new Vector3(0f, 0f, 15f);
    Vector3 limiteDer = new Vector3(0f, 0f, -15f);
    // (JUAN): Tiene que rotar en Z. Izq es positivo, Der es negativo.

    void Start()
    {
        esRotableIZQ = true;
        esRotableDER = true;
    }

    void Update()
    {
        if (balanza.transform.rotation.eulerAngles.z < limiteIzq.z)
        {
            esRotableIZQ = true;
        }
        if (balanza.transform.rotation.eulerAngles.z > limiteDer.z)
        {
            esRotableDER = true;
        }
        if (pesoIzq == true && pesoDer == false)
        {
            if (esRotableIZQ == true)
            {
                Debug.Log("IZQUIERDA");
                direccion = new Vector3(0f, 0f, 1f);
                transform.Rotate(velocidad * direccion * Time.deltaTime);
            }
            if (balanza.transform.rotation.eulerAngles.z >= limiteIzq.z)
            {
                esRotableIZQ = false;
                Debug.Log("Se paso de lado, el valor es de: " + balanza.transform.rotation.z.ToString());
            }
        }
        //if (pesoDer == true && pesoIzq == false)
        //{
        //    if (esRotableDER == true)
        //    {
        //        Debug.Log("DERECHA");
        //        direccion = new Vector3(0f, 0f, -1f);
        //        transform.Rotate(velocidad * direccion * Time.deltaTime);
        //    }
        //    if (balanza.transform.rotation.eulerAngles.z <= limiteDer.z)
        //    {
        //        esRotableDER = false;
        //        Debug.Log("Se paso de lado, el valor es de: " + balanza.transform.rotation.z.ToString());
        //    }
        //}

        

    }
}
