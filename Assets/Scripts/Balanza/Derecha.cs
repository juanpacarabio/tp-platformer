using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Derecha : MonoBehaviour
{
    public GameObject balanza;
    bool activo;
    Balanza script;
    private void Start()
    {
        script = balanza.GetComponent<Balanza>();
        activo = false;
    }
    void Update()
    {
        script.pesoDer = activo;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            activo = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            activo = false;
        }
    }
}
