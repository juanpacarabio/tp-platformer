﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public static Controller_Hud instance = null;

    public GameObject GLover;
    public GameObject Victory;
    public GameObject hud01;
    public GameObject hud02;
    public GameObject hud03;
    public GameObject hud04;
    public GameObject hud05;
    public GameObject hud06;
    public GameObject mini01;
    public GameObject mini02;
    public GameObject mini03;
    public GameObject mini04;
    public GameObject mini05;
    public GameObject mini06;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        Victory.SetActive(false);
        GLover.SetActive(false);
    }

    void Update()
    {
        if (GameManager.actualPlayer == 0)
        {
            MostrarJ1();
        }
        if (GameManager.actualPlayer == 1)
        {
            MostrarJ2();
        }
        if (GameManager.actualPlayer == 2)
        {
            MostrarJ3();
        }
        if (GameManager.actualPlayer == 3)
        {
            MostrarJ4();
        }
        if (GameManager.actualPlayer == 4)
        {
            MostrarJ5();
        }
        if (GameManager.actualPlayer == 5)
        {
            MostrarJ6();
        }
        //if(Target.playerOnTarget==true && GameManager.actualPlayer == 0)
        //{
        //    mini01.SetActive(true);
        //}
        //else
        //{
        //    mini01.SetActive(false);
        //}
        //if (Target.playerOnTarget == true && GameManager.actualPlayer == 1)
        //{
        //    mini02.SetActive(true);
        //}
        //else
        //{
        //    mini02.SetActive(false);
        //}
        //if (Target.playerOnTarget == true && GameManager.actualPlayer == 2)
        //{
        //    mini03.SetActive(true);
        //}
        //else
        //{
        //    mini03.SetActive(false);
        //}
        //if (Target.playerOnTarget == true && GameManager.actualPlayer == 3)
        //{
        //    mini04.SetActive(true);
        //}
        //else
        //{
        //    mini04.SetActive(false);
        //}
        //if (Target.playerOnTarget == true && GameManager.actualPlayer == 4)
        //{
        //    mini05.SetActive(true);
        //}
        //else
        //{
        //    mini05.SetActive(false);
        //}
        //if (Target.playerOnTarget == true && GameManager.actualPlayer == 5)
        //{
        //    mini06.SetActive(true);
        //}
        //else
        //{
        //    mini06.SetActive(false);
        //}


        if (GameManager.gameOver)
        {
            Time.timeScale = 0;
            GLover.SetActive(true);
            
        }
        if (GameManager.winCondition==true)
        {
            Time.timeScale = 0;
            Victory.SetActive(true);
            
        }
    }


    private void MostrarJ1()
    {
        hud01.gameObject.SetActive(true);
        hud02.gameObject.SetActive(false);
        hud03.gameObject.SetActive(false);
        hud04.gameObject.SetActive(false);
        hud05.gameObject.SetActive(false);
        hud06.gameObject.SetActive(false);
    }
    private void MostrarJ2()
    {
        hud01.gameObject.SetActive(false);
        hud02.gameObject.SetActive(true);
        hud03.gameObject.SetActive(false);
        hud04.gameObject.SetActive(false);
        hud05.gameObject.SetActive(false);
        hud06.gameObject.SetActive(false);
    }
    private void MostrarJ3()
    {
        hud01.gameObject.SetActive(false);
        hud02.gameObject.SetActive(false);
        hud03.gameObject.SetActive(true);
        hud04.gameObject.SetActive(false);
        hud05.gameObject.SetActive(false);
        hud06.gameObject.SetActive(false);
    }
    private void MostrarJ4()
    {
        hud01.gameObject.SetActive(false);
        hud02.gameObject.SetActive(false);
        hud03.gameObject.SetActive(false);
        hud04.gameObject.SetActive(true);
        hud05.gameObject.SetActive(false);
        hud06.gameObject.SetActive(false);
    }
    private void MostrarJ5()
    {
        hud01.gameObject.SetActive(false);
        hud02.gameObject.SetActive(false);
        hud03.gameObject.SetActive(false);
        hud04.gameObject.SetActive(false);
        hud05.gameObject.SetActive(true);
        hud06.gameObject.SetActive(false);
    }
    private void MostrarJ6()
    {
        hud01.gameObject.SetActive(false);
        hud02.gameObject.SetActive(false);
        hud03.gameObject.SetActive(false);
        hud04.gameObject.SetActive(false);
        hud05.gameObject.SetActive(false);
        hud06.gameObject.SetActive(true);
    }

   
}
