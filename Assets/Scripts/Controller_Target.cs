﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Target : MonoBehaviour
{
    public int targetNumber;

    public Controller_Hud HUD = Controller_Hud.instance;
    
    public bool playerOnTarget;


    private void Start()
    {
        playerOnTarget = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (other.GetComponent<Controller_Player>().playerNumber == targetNumber)
            {
                playerOnTarget = true;
                Debug.Log("Player: "+ other.GetComponent<Controller_Player>().playerNumber + " is on target number " + targetNumber);

                if (other.GetComponent<Controller_Player>().playerNumber == 0 && targetNumber == 0)
                {
                    HUD.mini01.SetActive(true);
                }
                if (other.GetComponent<Controller_Player>().playerNumber == 1 && targetNumber == 1)
                {
                    HUD.mini02.SetActive(true);
                }
                if (other.GetComponent<Controller_Player>().playerNumber == 2 && targetNumber == 2)
                {
                    HUD.mini03.SetActive(true);
                }
                if (other.GetComponent<Controller_Player>().playerNumber == 3 && targetNumber == 3)
                {
                    HUD.mini04.SetActive(true);
                }
                if (other.GetComponent<Controller_Player>().playerNumber == 4 && targetNumber == 4)
                {
                    HUD.mini05.SetActive(true);
                }
                if (other.GetComponent<Controller_Player>().playerNumber == 5 && targetNumber == 5)
                {
                    HUD.mini06.SetActive(true);
                }

            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (other.GetComponent<Controller_Player>().playerNumber == targetNumber)
            {
                playerOnTarget = false;
                Debug.Log("P off T");
                if (other.GetComponent<Controller_Player>().playerNumber == 0 && targetNumber == 0)
                {
                    HUD.mini01.SetActive(false);
                }
                if (other.GetComponent<Controller_Player>().playerNumber == 1 && targetNumber == 1)
                {
                    HUD.mini02.SetActive(false);
                }
                if (other.GetComponent<Controller_Player>().playerNumber == 2 && targetNumber == 2)
                {
                    HUD.mini03.SetActive(false);
                }
                if (other.GetComponent<Controller_Player>().playerNumber == 3 && targetNumber == 3)
                {
                    HUD.mini04.SetActive(false);
                }
                if (other.GetComponent<Controller_Player>().playerNumber == 4 && targetNumber == 4)
                {
                    HUD.mini05.SetActive(false);
                }
                if (other.GetComponent<Controller_Player>().playerNumber == 5 && targetNumber == 5)
                {
                    HUD.mini06.SetActive(false);
                }
            }
        }
    }

    
}
