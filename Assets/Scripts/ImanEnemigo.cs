using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImanEnemigo : MonoBehaviour
{
    public float fuerza = 200f;
    public List<Rigidbody> jugadores;
    public GameObject iman;
    Transform puntoiman;
    void Start()
    {
        puntoiman = iman.GetComponent<Transform>();
    }

    void FixedUpdate()
    {
        foreach (Rigidbody jugador in jugadores)
        {          
            jugador.AddForce((puntoiman.position - jugador.position) * fuerza * Time.fixedDeltaTime);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            jugadores.Add(other.attachedRigidbody);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            jugadores.Remove(other.attachedRigidbody);
        }
    }

}
