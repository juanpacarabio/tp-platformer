using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_ANGER : Controller_Player
{
    float holdJump = 0;
    float originalJumpForce;
    GameObject ultimoBreakable;
    bool saltoFull = false;
    protected override void Start()
    {
        base.Start();
        originalJumpForce = jumpForce;
    }
    public override void Jump()
    {
        if (IsOnSomething())
        {
            if (Input.GetKey(KeyCode.W))
            {
                if (holdJump < 2)
                {
                    holdJump = holdJump + Time.deltaTime;
                    jumpForce = jumpForce + Time.deltaTime * 6;
                } if (holdJump >= 2)
                {
                    holdJump = 0;
                    jumpForce = jumpForce * 2;
                    rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
                    jumpForce = originalJumpForce;
                    saltoFull = true;
                    if (ultimoBreakable != null)
                    {
                        Destroy(ultimoBreakable);
                    }
                }
            }
            if (Input.GetKeyUp(KeyCode.W))
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
                jumpForce = originalJumpForce;
                holdJump = 0;
            }
        }
    }

    public override void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);
        if (collision.gameObject.CompareTag("Breakable"))
        {
            if (saltoFull == true)
            {
                Destroy(collision.gameObject);
                saltoFull = false;
            }
            else if (saltoFull == false)
            {
                ultimoBreakable = collision.gameObject;
            }
        }
        if (collision.gameObject.CompareTag("Floor"))
        {
            saltoFull = false;
        }
    }

    public override void OnCollisionExit(Collision collision)
    {
        base.OnCollisionExit(collision);
        if (collision.gameObject.CompareTag("Breakable"))
        {
            ultimoBreakable = null;
        }
    }
}
