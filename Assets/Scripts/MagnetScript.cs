using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetScript : MonoBehaviour
{

    public float fuerza = 200f;
    public Rigidbody jugador;
    public GameObject moneda;
    Transform puntoiman;

    
    void Start()
    {
        puntoiman = moneda.GetComponent<Transform>();
    }

    private void FixedUpdate()
    {
        jugador.AddForce((puntoiman.position - jugador.position) * fuerza * Time.fixedDeltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player_06")
        {
            jugador = other.GetComponent<Rigidbody>();
        }
    }
}
