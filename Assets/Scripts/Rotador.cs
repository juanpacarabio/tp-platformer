using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotador : MonoBehaviour
{
    //public GameObject ventilador;
    //Transform transform;
    public float velocidad;
    Vector3 direccion = new Vector3(1f, 0f, 0f);
    //private void Start()
    //{
    //    transform = ventilador.GetComponent<Transform>();
    //}


    void Update()
    {
        transform.Rotate(velocidad * direccion * Time.deltaTime);   
    }
}
