using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sierra : MonoBehaviour
{
    public GameObject[] coordenadas;
    int coord_actual = 0;
    public float rapidez;
    float coor_espacio = 2;
    public float rotationSpeed = 50.0f;

    void Update()
    {

        if (Vector3.Distance(coordenadas[coord_actual].transform.position, transform.position) < coor_espacio) //si la distancia entre la coordenada y el objeto es menor al espacio que compone el punto
        {
            coord_actual++;
            if (coord_actual >= coordenadas.Length)
            {
                coord_actual = 0;
            }
        }
        transform.position = Vector3.MoveTowards(transform.position, coordenadas[coord_actual].transform.position, Time.deltaTime * rapidez);
        transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);

    }


    private void Girar()
    {
        this.gameObject.transform.Translate(Vector3.up * 10);
    }

    
}
