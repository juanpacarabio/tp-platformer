using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Viento : MonoBehaviour
{
    public float fuerza = 200f;
    public List<Rigidbody> jugadores;
    public GameObject ventilador;
    Transform puntoempuje;
    Vector3 direccion;
    void Start()
    {
        puntoempuje = ventilador.GetComponent<Transform>(); 
    }
    void FixedUpdate()
    {
        foreach (Rigidbody jugador in jugadores)
        {
            //direccion = new Vector3(jugador.position - ventilador.GetComponent<Rigidbody>().position).normalized;
            jugador.AddForce(-(puntoempuje.position - jugador.position) * fuerza * Time.fixedDeltaTime);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            jugadores.Add(other.attachedRigidbody);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            jugadores.Remove(other.attachedRigidbody);
        }
    }

}
